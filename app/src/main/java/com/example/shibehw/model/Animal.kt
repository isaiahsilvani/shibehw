package com.example.shibehw.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.shibehw.utils.AnimalType


@Entity(tableName = "animal_table")
data class Animal(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val type: AnimalType,
    val image: String
)