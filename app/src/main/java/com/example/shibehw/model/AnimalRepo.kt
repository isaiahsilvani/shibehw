package com.example.shibehw.model

import android.content.Context
import android.util.Log
import com.example.shibehw.model.local.AnimalDB
import com.example.shibehw.model.local.AnimalDao
import com.example.shibehw.model.remote.AnimalService
import com.example.shibehw.utils.AnimalType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class AnimalRepo(context: Context) {

    private val animalService = AnimalService.getInstance()
    private val animalDao = AnimalDB.getInstance(context).animalDao()

    suspend fun getAnimal(animalType: AnimalType) = withContext(Dispatchers.IO) {

        val cachedAnimals = animalDao.getAnimal(animalType)

        return@withContext cachedAnimals.ifEmpty {

            val remoteAnimals = when(animalType) {
                AnimalType.DOG -> animalService.getDogs()
                AnimalType.CAT -> animalService.getCats()
                AnimalType.BIRD -> animalService.getBirds()
            }

            val entities: List<Animal> = remoteAnimals.map {
                Log.e("TAG", it.toString())
                Animal(type = animalType, image = it)
            }
            animalDao.insertAnimal(*entities.toTypedArray())

            return@ifEmpty entities
        }
    }
}