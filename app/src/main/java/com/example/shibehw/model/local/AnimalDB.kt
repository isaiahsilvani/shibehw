package com.example.shibehw.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.shibehw.model.Animal

@Database(entities = [Animal::class], version = 1)
abstract class AnimalDB : RoomDatabase() {

    abstract fun animalDao(): AnimalDao

    companion object {
        private const val DATABASE_NAME = "animal.db"

        @Volatile
        private var instance: AnimalDB? = null

        fun getInstance(context: Context): AnimalDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): AnimalDB {
            return Room
                .databaseBuilder(context, AnimalDB::class.java, DATABASE_NAME)
                .build()
        }

    }

}