package com.example.shibehw.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shibehw.model.Animal
import com.example.shibehw.utils.AnimalType

@Dao
interface AnimalDao {
    // Get the animals
    @Query("SELECT * FROM animal_table WHERE type = :animalType")
    suspend fun getAnimal(animalType: AnimalType): List<Animal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnimal(vararg animal: Animal)
}