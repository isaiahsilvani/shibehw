package com.example.shibehw.model.remote

import com.example.shibehw.model.Animal
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface AnimalService{

    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val ENDPOINT_DOG = "/api/shibes"
        private const val ENDPOINT_CAT = "/api/cats"
        private const val ENDPOINT_BIRD = "/api/birds"

        fun getInstance(): AnimalService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()

    }
    @GET(ENDPOINT_DOG)
    suspend fun getDogs(@Query("count") count: Int = 10): List<String>

    @GET(ENDPOINT_CAT)
    suspend fun getCats(@Query("count") count: Int = 10): List<String>

    @GET(ENDPOINT_BIRD)
    suspend fun getBirds(@Query("count") count: Int = 10): List<String>
}