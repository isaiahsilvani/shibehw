package com.example.shibehw.utils

enum class AnimalType {
    DOG, CAT, BIRD
}