package com.example.shibehw.view.BirdsFragment

import com.example.shibehw.model.Animal

data class BirdState(
    val isLoading: Boolean = false,
    val birds: List<Animal> = emptyList()
)