package com.example.shibehw.view.BirdsFragment

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibehw.databinding.FragmentBirdsBinding
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.view.AnimalAdapter
import com.example.shibehw.viewmodel.BirdViewModel.BirdVMFactory
import com.example.shibehw.viewmodel.BirdViewModel.BirdViewModel
import com.example.shibehw.viewmodel.DogViewModel.DogVMFactory

class BirdsFragment: Fragment() {

    lateinit var binding: FragmentBirdsBinding
    private lateinit var vmFactory: BirdVMFactory
    private val viewModel: BirdViewModel by viewModels() { vmFactory }

    private val animalAdapter: AnimalAdapter by lazy {
        AnimalAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBirdsBinding.inflate(layoutInflater)
        vmFactory = BirdVMFactory(repo = AnimalRepo(requireContext()))
        // Set up adapter and layout manager for recycler view
        with(binding.rvBirds) {
            layoutManager = GridLayoutManager(this@BirdsFragment.context, 2)
            adapter = animalAdapter
        }
        initObservers()
        viewModel.getBirds()
        return binding.root
    }

    private fun initObservers() {
        viewModel.birdState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                // Display progress bar if loading
                progressBar.isVisible = state.isLoading
                // Turn buttons off if loading
                if (state.isLoading) {
                    turnOffButtons()
                } else {
                    turnOnButtons()
                }
                animalAdapter.updateList(state.birds)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            // navigation for Buttons
            val buttons = listOf(btnCats, btnDogs)
            for (button in buttons) {
                button.setOnClickListener {
                    val action = when {
                        button == btnDogs -> BirdsFragmentDirections.actionBirdsFragmentToDoggosFragment()
                        button == btnCats -> BirdsFragmentDirections.actionBirdsFragmentToKittiesFragment()
                        else -> { throw error("I don't know man... crap :/") }
                    }
                    findNavController().navigate(action)
                }
            }

        }
    }
    private fun turnOffButtons() {
        with(binding) {
            btnCats.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
            btnCats.setOnClickListener(null)
            btnDogs.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
            btnDogs.setOnClickListener(null)
        }
    }

    private fun turnOnButtons() {
        with(binding) {
            val buttons = listOf(btnCats, btnDogs)
            for (button in buttons) {
                button.setOnClickListener {
                    val action = when {
                        button == btnCats -> BirdsFragmentDirections.actionBirdsFragmentToKittiesFragment()
                        button == btnDogs -> BirdsFragmentDirections.actionBirdsFragmentToDoggosFragment()
                        else -> { throw error("I don't know man... crap :/") }
                    }
                    findNavController().navigate(action)
                }
                button.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
            }
        }
    }
}