package com.example.shibehw.view.DoggosFragment

import com.example.shibehw.model.Animal

data class DogState(
    val isLoading: Boolean = false,
    val dogs: List<Animal> = emptyList()
)