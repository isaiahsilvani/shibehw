package com.example.shibehw.view.DoggosFragment

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibehw.databinding.FragmentDoggosBinding
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.view.AnimalAdapter
import com.example.shibehw.viewmodel.DogViewModel.DogVMFactory
import com.example.shibehw.viewmodel.DogViewModel.DogViewModel

class DoggosFragment : Fragment() {

    lateinit var binding: FragmentDoggosBinding

    private lateinit var vmFactory: DogVMFactory
    // todo -- Problem
    private val viewModel by viewModels<DogViewModel>() { vmFactory }

    private val animalAdapter: AnimalAdapter by lazy {
        AnimalAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDoggosBinding.inflate(layoutInflater)
        // set up adapter and layout manager
        vmFactory = DogVMFactory(repo = AnimalRepo(requireContext()))
        with(binding.rvDogs) {
            layoutManager = GridLayoutManager(this@DoggosFragment.context, 2)
            adapter = animalAdapter
        }
        initObservers()
        viewModel.getDogs()
        return binding.root
    }

    private fun initObservers() {
        viewModel.dogState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                // Display progress bar if loading
                progressBar.isVisible = state.isLoading
                // Turn buttons off if loading
                if (state.isLoading) {
                    turnOffButtons()
                } else {
                    turnOnButtons()
                }
                Log.e("STATE", state.dogs.toString())
                animalAdapter.updateList(state.dogs)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            // navigation for Buttons
            val buttons = listOf(btnCats, btnBirds)
            for (button in buttons) {
                button.setOnClickListener {
                    val action = when {
                        button == btnBirds -> DoggosFragmentDirections.actionDoggosFragmentToBirdsFragment()
                        button == btnCats -> DoggosFragmentDirections.actionDoggosFragmentToKittiesFragment()
                        else -> { throw error("I don't know man... crap :/") }
                    }
                    findNavController().navigate(action)
                }
            }

        }
    }

    private fun turnOffButtons() {
        with(binding) {
            btnCats.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
            btnCats.setOnClickListener(null)
            btnBirds.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
            btnBirds.setOnClickListener(null)
        }
    }

    private fun turnOnButtons() {
        with(binding) {
            val buttons = listOf(btnCats, btnBirds)
            for (button in buttons) {
                button.setOnClickListener {
                    val action = when {
                        button == btnCats -> DoggosFragmentDirections.actionDoggosFragmentToKittiesFragment()
                        button == btnBirds -> DoggosFragmentDirections.actionDoggosFragmentToBirdsFragment()
                        else -> { throw error("I don't know man... crap :/") }
                    }
                    findNavController().navigate(action)
                }
                button.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
            }
        }
    }
}