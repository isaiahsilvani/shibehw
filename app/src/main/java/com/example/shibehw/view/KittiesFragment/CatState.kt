package com.example.shibehw.view.KittiesFragment

import com.example.shibehw.model.Animal

data class CatState(
    val isLoading: Boolean = false,
    val cats: List<Animal> = emptyList()
)