package com.example.shibehw.view.KittiesFragment


import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibehw.databinding.FragmentKittiesBinding
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.view.AnimalAdapter
import com.example.shibehw.viewmodel.CatViewModel.CatVMFactory
import com.example.shibehw.viewmodel.CatViewModel.CatViewModel
import com.example.shibehw.viewmodel.DogViewModel.DogVMFactory

class KittiesFragment: Fragment() {

    lateinit var binding: FragmentKittiesBinding

    private lateinit var vmFactory: CatVMFactory
    private val viewModel: CatViewModel by viewModels() { vmFactory }

    private val animalAdapter: AnimalAdapter by lazy {
        AnimalAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentKittiesBinding.inflate(layoutInflater)
        vmFactory = CatVMFactory(repo = AnimalRepo(requireContext()))
        // set up adapter and layout manager
        with(binding.rvCats) {
            layoutManager = GridLayoutManager(this@KittiesFragment.context, 2)
            adapter = animalAdapter
        }
        viewModel.getCats()
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.catState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                // Display progress bar if loading
                progressBar.isVisible = state.isLoading
                // Turn buttons off if loading
                if (state.isLoading) {
                    turnOffButtons()
                } else {
                    turnOnButtons()
                }
                // throw the list into the adapter
                Log.e("KitFrag", state.cats.toString())
                animalAdapter.updateList(state.cats)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            turnOnButtons()
    }

    private fun turnOffButtons() {
        with(binding) {
            btnDogs.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
            btnDogs.setOnClickListener(null)
            btnBirds.backgroundTintList = ColorStateList.valueOf(Color.GRAY)
            btnBirds.setOnClickListener(null)
        }
    }

    private fun turnOnButtons() {
        with(binding) {
            val buttons = listOf(btnDogs, btnBirds)
            for (button in buttons) {
                button.setOnClickListener {
                    val action = when {
                        button == btnDogs -> KittiesFragmentDirections.actionKittiesFragmentToDoggosFragment2()
                        button == btnBirds -> KittiesFragmentDirections.actionKittiesFragmentToBirdsFragment2()
                        else -> { throw error("I don't know man... crap :/") }
                    }
                    findNavController().navigate(action)
                }
                // todo fix the color jawn
                button.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
            }
        }

    }

}