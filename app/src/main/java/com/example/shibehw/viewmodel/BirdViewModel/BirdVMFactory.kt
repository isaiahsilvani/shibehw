package com.example.shibehw.viewmodel.BirdViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibehw.model.AnimalRepo

class BirdVMFactory(
    private val repo: AnimalRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return BirdViewModel(repo) as T
    }
}