package com.example.shibehw.viewmodel.BirdViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.utils.AnimalType
import com.example.shibehw.view.BirdsFragment.BirdState
import kotlinx.coroutines.launch

class BirdViewModel(private val repo: AnimalRepo) : ViewModel() {

    // A data class can't be instantiated
    private val _birdState: MutableLiveData<BirdState> = MutableLiveData(BirdState())
    val birdState: LiveData<BirdState> get() = _birdState

    fun getBirds(count: Int = 10) {
        viewModelScope.launch {
            _birdState.value = BirdState(isLoading = true)
            Log.e("BirdVM", _birdState.value.toString())
            val result = repo.getAnimal(AnimalType.BIRD)
            _birdState.value = BirdState(birds = result)
            Log.e("BirdVM", _birdState.value.toString())
        }
    }
}