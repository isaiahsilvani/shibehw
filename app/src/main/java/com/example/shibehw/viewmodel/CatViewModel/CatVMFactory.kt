package com.example.shibehw.viewmodel.CatViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibehw.model.AnimalRepo

class CatVMFactory(
    private val repo: AnimalRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CatViewModel(repo) as T
    }
}