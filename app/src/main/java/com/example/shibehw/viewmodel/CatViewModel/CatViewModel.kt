package com.example.shibehw.viewmodel.CatViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.utils.AnimalType
import com.example.shibehw.view.KittiesFragment.CatState
import kotlinx.coroutines.launch

class CatViewModel(val repo: AnimalRepo) : ViewModel() {

    // A data class can't be instantiated
    private val _catState: MutableLiveData<CatState> = MutableLiveData(CatState())
    val catState: LiveData<CatState> get() = _catState

    fun getCats(count: Int = 10) {
        viewModelScope.launch {
            _catState.value = CatState(isLoading = true)
            val result = repo.getAnimal(AnimalType.CAT)
            _catState.value = CatState(cats = result, isLoading = false)
            Log.e("CatVM", _catState.value.toString())
        }
    }
}