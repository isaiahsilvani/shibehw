package com.example.shibehw.viewmodel.DogViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.viewmodel.CatViewModel.CatViewModel

class DogVMFactory(
    private val repo: AnimalRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DogViewModel(repo) as T
    }
}

