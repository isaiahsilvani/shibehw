package com.example.shibehw.viewmodel.DogViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibehw.model.AnimalRepo
import com.example.shibehw.utils.AnimalType
import com.example.shibehw.view.DoggosFragment.DogState
import kotlinx.coroutines.launch

class DogViewModel(private val repo: AnimalRepo) : ViewModel() {

    // A data class can't be instantiated
    private val _dogState: MutableLiveData<DogState> = MutableLiveData(DogState())
    val dogState: LiveData<DogState> get() = _dogState

    fun getDogs(count: Int = 10) {
        viewModelScope.launch {
            _dogState.value = DogState(isLoading = true)
            val result = repo.getAnimal(AnimalType.DOG)
            _dogState.value = DogState(dogs = result, isLoading = false)
            Log.e("DogVM", _dogState.value.toString())
        }
    }
}